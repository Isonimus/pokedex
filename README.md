# Vue 3 + TypeScript PokeApi Client

This project is a client for the PokeApi, built using Vue 3's Composition API and Bootstrap. It displays Pokemon included in the first generation as cards on the screen and allows users to filter them by name using a search string or by types using a type selection GUI. The results are displayed paginated, in batches of 10 Pokemon.

## Installation

To install the project, follow these steps:


```bash
# Clone the repository
git clone https://gitlab.com/Isonimus/pokedex.git

# Change into the project directory
cd pokedex

# Install dependencies
npm install
```

## Usage

```bash
# To serve the project locally for development, run:
npm run serve

# To build the project for production, run:
npm run build # This will generate a production-ready build in the `dist` directory.
```