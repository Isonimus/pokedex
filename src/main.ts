import { createApp } from 'vue'
import App from './App.vue'
import pokeService from './services/pokeService'
import 'bootstrap/dist/css/bootstrap.min.css';

createApp(App)
  .provide('pokeService', pokeService)
  .mount('#app');
