import { PokeType, Pokemon } from "@/types/pokemon";
import { config } from "@/config/config"

export const EVENTS = {
  NAMES_LOADED: "names_loaded",
  TYPES_LOADED: "types_loaded"
}

export const POKEMON_TYPE_WILDCARD = "all";

export class PokeService {
  private cache: Map<string, Pokemon>;
  private pokeTypes: string[];

  constructor(){
    this.cache  = new Map();
    this.pokeTypes = [];
    this.initService();
  }

  private initService(){
    this.loadInitialData();
  }

  async loadInitialData(){
    fetch(config.BASE_URL)
      .then((response: any)=>{
        return response.json();
      })
      .then((response: any)=>{
        this.loadPokemonData(response.pokemon_species);
        this.loadPokeTypeData(response.types);
      })
      .catch((error: any)=>{
        console.error(error);
        throw(error);
      });
  }

  loadPokemonData(pokemonSpecies: any[]){
    const dataPromises = pokemonSpecies.map((element: any) => {
      return fetch(element.url)
        .then((response) => response.json())
        .then((json) => {
          this.cache.set(element.name, {
            pokeName: element.name,
            pokeData: {
              order: json.id,
              color: { name: json.color.name },
              types: []
            }
          });
        })
        .catch((error) => {
          console.error(error);
        });
    });

    Promise.all(dataPromises).then(()=>{
      window.dispatchEvent(new CustomEvent(EVENTS.NAMES_LOADED));
    })
  }

  loadPokeTypeData(types: PokeType[]){
    const typePromises = types.map((type: PokeType) => {
      return fetch(type.url)
      .then((response) => response.json())
      .then((json) => {
        const typeToAdd = json.name;
        this.pokeTypes.push(typeToAdd);
        json.pokemon.forEach((pokeData: any)=>{
          const pokemonToUpdate = this.cache.get(pokeData.pokemon.name);
          if(pokemonToUpdate){
            pokemonToUpdate.pokeData.types.push(typeToAdd);
          }
        })
      })
      .catch((error) => {
        console.error(error);
      });
    });

    Promise.all(typePromises).then(()=>{
      window.dispatchEvent(new CustomEvent(EVENTS.TYPES_LOADED));
    });
  }

  getPokeNames(){
    return Array.from(this.cache.keys()).sort();
  }

  getPokeTypes() : string[]{
    return this.pokeTypes.sort();
  }

  getFilteredPokemon(limit: number = 100, skip: number = 0, filter: string = "", types: string[] = []){
    return Array.from(this.cache.values()).filter((entry: Pokemon)=>{
      return entry.pokeName.includes(filter);
    }).filter((entry: Pokemon)=>{
      if(types.includes(POKEMON_TYPE_WILDCARD)){
        return true;
      }else{
        return types.reduce((acc, cur)=>{
          return acc += entry.pokeData.types.includes(cur) ? 1 : 0;
        }, 0) === types.length
      }
    })
    .sort((entryA: Pokemon, entryB: Pokemon)=> entryA?.pokeData?.order - entryB?.pokeData?.order)
    .slice(skip, skip + limit)
  }

  getQueryResultLength(limit: number = 100, skip: number = 0, filter: string = "", types: string[] = []){
    return Array.from(this.cache.values()).filter((entry: Pokemon)=>{
      return entry.pokeName.includes(filter);
    }).filter((entry: Pokemon)=>{
      if(types.includes(POKEMON_TYPE_WILDCARD)){
        return true;
      }else{
        return types.reduce((acc, cur)=>{
          return acc += entry.pokeData.types.includes(cur) ? 1 : 0;
        }, 0) === types.length
      }
    })
    .length
  }
}

export default new PokeService();