export interface PokeData {
  order: number,
  color: {
    name: string
  },
  types: string[]
}

export type Pokemon = {
  pokeName: string,
  pokeData: PokeData
}

export type PokeType = {
  name: string,
  url: string
}