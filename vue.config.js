const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    //PROXY TO AVOID CORS ERROR
    proxy: {
      '/api': {
        target: 'https://pokeapi.co/api/',
        changeOrigin: true,
        pathRewrite: { '^/api': '' },
      },
    },
  },
});
